ARG CMS_PATH=/cvmfs/cms.cern.ch
ARG CMSSW_RELEASE=CMSSW_10_6_30
ARG SCRAM_ARCH=slc7_amd64_gcc700
#FROM gitlab-registry.cern.ch/cms-cloud/cmssw-docker/al9-cms
#FROM gitlab-registry.cern.ch/cms-cloud/cmssw_10_6_30-slc7_amd64_gcc700
FROM cmsopendata/cmssw_10_6_30-slc7_amd64_gcc700
RUN pwd;ls
RUN git clone -b radiation https://github.com/enibigir/SUSYBSMAnalysis-HSCP.git SUSYBSMAnalysis

WORKDIR /code/${CMSSW_RELEASE}/src/SUSYBSMAnalysis
RUN pwd;ls
RUN scram b -j4

WORKDIR /code/${CMSSW_RELEASE}/src/SUSYBSMAnalysis/Analyzer/test/Emery

RUN source fresh_start.sh